(function($) {
  var rand = Math.floor(Math.random() * 99999999);

  function Advert(options) {
    // define class members
    this.old_width = false;

    // handle configuration, these options are required
    this.el = options.el;
    this.ad_type = options.type;
    this.iframe_url = options.iframeUrl;
    this.debug = options.debug;

    // Ad tag parameters
    this.path = options.path;
    this.ptype = options.ptype;
    this.pos = options.pos;
    this.tile = options.tile;

    // Screen size [s|m|l], calculate this on render
    this.ss = false;

    // Registered user data
    // TODO: Look for registration object and user status
    this.rg = options.rg || 'ur'; // r (registered) or ur (unregistered)
    //this.by = options.by || ''; // birth year
    //this.zc = options.zc || ''; // zipcode
    //this.gr = options.gr || ''; // gender [m|f]
    //this.ic = options.ic || ''; // ???

    // TODO: Figure out wtf this stuff is for
    //this.mob = options.mob || 'js'; // mobile only
    //this.iab = options.iab || '';
    //this.ref = options.ref || '';
    //this.rs = options.rs || []; // there are usually many of these

    // Build the iframe we'll put the ad in
    this.$el = $(this.el);
    this.$iframe = $(document.createElement('IFRAME'))
      .attr({
        width: 0,
        height: 0,
        scrolling: 'no',
        seamless: true,
        frameBorder: 0
      })
      .appendTo(this.$el);

    // Render the ad, setup events
    this.render();

    var instance = this;

    // re-render this advert on resize
    $(window).resize(function() {instance.render();});

    // Did the ad really load?
    this.$iframe.load(function() {
      if (instance.$iframe.contents().find('#div-gpt-ad').height()) {
        instance.$el.removeClass('hidden');
      } else {
        instance.$el.addClass('hidden');
      }
    });
    this.ads_loaded = true;
  }

  Advert.prototype.render = function() {
    var width = $(window).width();
    this.ss = 'l';

    if ( !this.old_width ||
        (width > 766 && this.old_width < 767) ||
        (width < 767 && this.old_width > 766) ||
        (width <= 320 && this.old_width > 320) ) {

      if (width > 767) {
        if (this.ad_type == 'cube') {
          this.width = 300;
          this.height = 250;
        } else {
          this.width = 728;
          this.height = 90;
        }
      } else {
        this.ss = 'm';
        this.width = 320;
        this.height = 50;
      }

      if ( width <= 320 )
        this.ss = 's';

      // Render the ad
      var ad_url = this.makeIframeUrl();
      this.$iframe
        .attr({
          width: this.width,
          height: this.height,
          src: ad_url
        });
      this.$iframe[0].contentWindow.location.replace(ad_url);
    }
    this.old_width = width;
  };

  Advert.prototype.makeIframeUrl = function() {
    // Build query string with pos, tile, ptype if specified
    var ret = false,
        param_string = '',
        params = {
          ptype: this.ptype,
          tile: this.tile, // tile needs to be 1-indexed
          pos: this.pos,
          rg: this.rg
        };
    if(this.iab)
      params.iab = this.iab;

    // visitor is registered, so add reg data to ad request
    if(this.rg === 'r') {
      params.by = this.by;
      params.zc = this.zc;
      params.gr = this.gr;
      params.ic = this.ic;
    }

    params.ss = this.ss;
    //params.mob = this.mob;

    param_string = $.param(params);
   ret = this.iframe_url + "?iu=4011%2Ftrb.sunsentinel%2F" +
      encodeURIComponent(this.path) + '&' + param_string +
      "&sz=" + this.width + "x" + this.height +
     "&ord=" + rand + "&c=" + rand + "&ss=" + this.ss;

    if(this.debug) console.log(ret);
    return ret;
  };

  Advert.prototype.refresh = function() {
    if (!this.ads_loaded)
      return;

    var ad_url = this.makeIframeUrl();
    this.$iframe.attr('src', ad_url);
    this.$iframe[0].contentWindow.location.replace(ad_url);
    this.$el.trigger('refresh');
  };

  var is_mobile = function() {
    return ($(window).width() < 768);
  };

  var tile_index = 0,
      tile_count = 0,
      cube_index = 0;

  $.fn.ad = function(options) {
    if(typeof options === 'string') {
      if(options === 'refresh') {
        if(this.length > 0) {
          rand = Math.floor(Math.random() * 99999999);
          $('body').trigger('ad-refresh');
        }
        return this.each(function() {
          var view = $(this).data('advert-view');
          view.refresh();
        });
      }
    } else {
      if(typeof options === 'undefined')
        options = {};

      if(typeof options.path === 'undefined') {
        if(typeof $('body').data('ad-path') !== 'undefined')
          options.path = $('body').data('ad-path');
        else
          throw "ads.js: Missing ad path";
      }

      if(typeof options.ptype  === 'undefined') {
        if(typeof $('body').data('ad-ptype') !== 'undefined')
          options.ptype = $('body').data('ad-ptype');
        else
          throw "ads.js: Missing page type";
      }

      if(typeof options.iframeUrl  === 'undefined') {
        if(typeof $('body').data('ad-iframe-url') !== 'undefined')
          options.iframeUrl = $('body').data('ad-iframe-url');
        else
          options.iframeUrl = $.fn.ad.defaults.iframeUrl;
      }

      tile_count = this.length;

      $('body').on('ad-refresh', function() {
        // Toggle an omniture PV
        if(typeof(s) === 'object') {
          s.events = "";
          s.t();
        }
      });

      // process the selected elements
      return this.each(function(i, ele) {
        var view_options = {
          el: this,
          iframeUrl: options.iframeUrl,
          type: options.type || $(this).data('ad-type') || $.fn.ad.defaults.type,
          path: options.path,
          ptype: options.ptype,
          debug: options.debug || $.fn.ad.defaults.debug
        };

        tile_index++;
        view_options.tile = tile_index;

        // Here we're going to try to guess some extra options to pass to
        // the ad tags

        if(view_options.type === 'leaderboard') {
          // If this leaderboard is at the top of the page, pos=T&dcopt=ist
          if(i === 0)
            view_options.pos = 'T';

          // If this leaderboard is at the bottom of the page, pos=b
          if(i === tile_count)
            view_options.pos = 'b';
        }

        if(view_options.type === 'cube') {
          // The first cube on the page should be pos=1
          // The second cube on the page might be pos=10 or pos=15
          // TODO: figure out a global pattern, till then, we'll make one up
          cube_index++;
          view_options.pos = cube_index;
        }

        $(this).data(
          'advert-view',
          new Advert(view_options));
      });
    }
  };

  $.fn.ad.defaults = {
    // These are the defaults.
    type: 'leaderboard',
    iframeUrl: 'static/ad-iframe.html',
    debug: false
  };

  $(document).ready(function() {
    $('.advert').ad();
  });

})(jQuery);
